package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsLog;

public interface UpmsLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsLog record);

    int insertSelective(UpmsLog record);

    UpmsLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsLog record);

    int updateByPrimaryKeyWithBLOBs(UpmsLog record);

    int updateByPrimaryKey(UpmsLog record);
}